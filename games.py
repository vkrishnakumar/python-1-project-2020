"""
contains all games used
"""
from header import tenwords as words
from header import eladict, color
from header import tenwordexamples as examples
from header import tenwordquestions as questions
from header import tenwordanswers as correctanswer
from header import tenwordchoices as choices
# importing things from the header.py file
# import as lets you call that function under a different name
import random 

def guessgame(): # guessing game that we used before
  secretwordstring = random.choice(words)
  secret_word = list(secretwordstring)
  guesses_left = 10
  dashes = ["-" for x in range(len(secret_word))] # sets dashes variable to # of letters in word

  def get_guess(): # gets the user's guess and sees if it is a lowercase letter
      while True:
          print(''.join(dashes))
          userinput = input("Enter a letter: ")
          if not userinput.islower():
              print("Your guess must be a lowercase letter!")
          elif len(userinput) != 1:
              print("Your guest must have exactly one character!")
          else:
              return userinput


  def update_dashes(): # updates dashes
      for i, letter in enumerate(secret_word): # i is the position of the word
          if letter == userguess:
              dashes[i] = userguess # if user guesses letter, change position of letter in dashes to the letter

  
  # defining the dashes list, lists are easier to use than string

  # joining list items
  #print(''.join(dashes))

 
  while guesses_left != 0 and "-" in dashes: 
    # if they havent guessed the word and still have guesses
      print("\n\n______________________________________________________" + color.BOLD + color.UNDERLINE + "\nDefinition of word:" + color.END + "\n\n" + eladict[secretwordstring])

      print("______________________________________________________")
      print(str(guesses_left) + " incorrect guesses left.")
      userguess = get_guess()
      if userguess in secret_word:
          print("That letter is in the ELA word!")
          update_dashes()
      else:
          print("That letter is not in the word.")
          guesses_left = guesses_left - 1
      #print(''.join(dashes))
  print("______________________________________________________")
  if guesses_left == 0: # if out of guesses
      print("You lose!\nThe word was: " + str(''.join(secret_word)))
  elif "-" not in dashes: # if completed the word
      print("You win!\nThe word was: " + str(''.join(secret_word))+ "\n\n\n")  


def quizgame(): # quizzes the user on 10 words
  while True: 
    correctanswers = 0
    wronganswers = 0 # defining important variables
    for word in words: # for keys in the ela list
      print(color.BOLD + word.title() + " Definition: " + color.END + eladict[word]) # prints definition
      print("\n" + color.BOLD + word.title() + " Example: " + color.END + examples[word] + "\n\n") # prints an example of the word being used
      for i in range(2): # 2 questions for each word
        if i == 1: # if it's about to print the second question go to the next line
          print("\n\n")
        print(color.UNDERLINE + questions[word][i] + color.END) # print question
        print(choices[word][i]) # print the multiple coices
        while True:
          useranswer = input("Which choice is correct(a,b,c)? ")
          if len(useranswer) != 1: # if user guess is only one character
            print("Your guest must have exactly one character!")
            continue
          elif useranswer.lower() == correctanswer[word][i]: # if user chose the correct answer letter
            correctanswers = correctanswers + 1 
            print(color.BOLD + color.UNDERLINE + "\nCorrect." + color.END)
            break
          else: # if the user chose the wrong answer
            wronganswers = wronganswers + 1
            print(color.BOLD + color.UNDERLINE + "\nThat is incorrect." + color.END)
            break
      print("\n\n\n")
    totalquestions = correctanswers + wronganswers
    wrongpercent = (100/totalquestions)*correctanswers # calculating how many were correct
    print(color.BOLD + "Your Score: "+ color.END + color.UNDERLINE + str(wrongpercent) + "%" + color.END)
    if wrongpercent < 75:
      input("Please get a 75% or higher to continue (Press ENTER to retry.) ")
      continue # if the user got lower than 75% repeat
    else:
      print("Congratulations!! You got a 75% or above!\n\n") # congratulate the user if they passed
      break