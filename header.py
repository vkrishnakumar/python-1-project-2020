"""
This file contains all functions and variables that will be used in other files
"""

class color:  # creates a class that we can use (color.ETC)
    PURPLE = '\033[95m'  # weird color codes
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'  # all new text will now by normal


fullwordlist = [ # list of all words used
    "able", "Adjective Phrase", "Caption", "Comparative Adjective",
    "Conflict Resolution", "Descriptive Language", "dis", "Editorial", "er",
    "est", "Fact vs opinion", "Foreshadowing", "Interjection", "Interview",
    "Italics", "less", "ly", "mis", "non", "Paraphrase", "Personification",
    "Plot Development", "Predicate Adjective", "Relevant supporting details",
    "Salutation", "Sentence combining", "Stereotype", "Superlative adjective",
    "Synonym", "Verb Phrase"
]

eladict = { # dictionary with all words
    "able":
    "Suffix meaning \"Capable of\"",
    "adjective phrase":
    "Words that describe or modify other words, making your writing and speaking much more specific, and a whole lot more interesting.",
    "caption":
    "A title or brief explanation appended to an article, illustration, cartoon, or poster.",
    "comparative adjective":
    "Adjectives that are used to compare nouns.",
    "conflict resolution":
    "The steps and processes used to end a conflict.",
    "descriptive language":
    "Adding more details to text. Adds more purpose, emotion and character.",
    "dis":
    "Prefix meaning \"Apart\"",
    "editorial":
    "Article that gives the opinion of the editor or publisher.",
    "er":
    "Used to compare adjectives (weirder, snowier) and to describe somebody who performs an action (writer)",
    "est":
    "Suffix that adds the meaning of \"most\" to words",
    "fact vs opinion":
    "Facts can be proven to be true or false, opinion cannot.",
    "foreshadowing":
    "Warning of a future event.",
    "interjection":
    "Remark that expresses a feeling or reaction.",
    "interview":
    "Structured meeting where one party asks questions and the other party provides answers.",
    "italics":
    "Used to put emphasis on words, slants text to the right.",
    "less":
    "Suffix that means \"Without\".",
    "ly":
    "Suffix that turns an adjective into an adverb.",
    "mis":
    "Prefix that means \"opposite\".",
    "non":
    "Prefix that means \"not\" or \"opposite\".",
    "paraphrase":
    "Rephrasing what someone else has said or written.",
    "personification":
    "Giving human characteristics to animals or inanimate objects.",
    "plot development":
    "When the plot thickens and moves forward.",
    "predicate adjective":
    "A word that modifies or describes a subject.",
    "relevant supporting details":
    "Examples that support the main idea of a text.",
    "salutation":
    "A formal greeting.",
    "sentence combining":
    "Goining two or more short sentences into a longer sentence.",
    "stereotype":
    "A widely held but oversimplified image or idea of a particular person or thing.",
    "superlative adjective":
    "Of the highest quality of degree.",
    "synonym":
    "A word or phrase that means exactly or nearly the same as another word or phrase in the same language.",
    "verb phrase":
    "The part of a sentence containing the verb and any direct or indirect object, but not the subject."
}

tenwordquestions = { # contains all questions
    "caption": [
        "Which of the following is the BEST caption?",
        "Which of the following is the WORST caption?"
    ],
    "synonym": [
        "Which of the following is a synonym of smart?",
        "Which of the following is NOT a synonym of smart?"
    ],
    "salutation": [
        "Which of the following is a formal salutation?",
        "Which of the following would be appropriate when addressing a job interviewer?"
    ],
    "paraphrase": [
        "What does paraphrasing do to a paragraph that already exists?",
        "An alternative to paraphrasing is:"
    ],
    "stereotype":[
        "Which of the following is a sterotype?",
        "Why do stereotypes exist?"
    ],
    "foreshadowing": [
        "Which of the following is the correct way to foreshadow?",
        "How is foreshadowing used?"
    ],
    "personification": [
        "Which answer is the correct use of personification?",
        "Why is personification used?"
    ],
    "interview": [
        "In an interview, what is the correct way of greeting?",
        "Which option is the definition of Interview?"
    ],
    "non": [
        "How do you use the prefix non in a word?",
        "What word in the options below uses the prefix non?"
    ],
    "less": [
        "What word in the options below uses the suffix less?",
        "What does the suffix less mean when you're using it in a word?"
    ]
}

tenwordchoices = { # contains all multiple choices
    "caption": [
        "a. The dog is eating ice cream\nb. This husky (left) is eating his owner's ice cream.\nc. dog ice cream",
        "a. Man on the street\nb. The man pictured is walking on the sidewalk.\nc. Man walking on a sidewalk.\n"
    ],
    "synonym": [
        "a. Intelligent.\nb. Food.\nc. Carpet\n",
        "a. Clever.\nb. Bright.\nc. Dense.\n"
    ],
    "salutation": [
        "a. Yours Truly.\nb. To Whom it may Concern,\nc. Hi,\n",
        "a. Hello Mr.Swag. nice to meet you!\nb. Ayoo wassup bro!\nc. Hey, Mr.Swag, whats good dude!\n"
    ],
    "paraphrase": [
        "a. It rewrites the paragraph with basically all of the information on there.\nb. It makes a weird sound.\nc. The paragraph becomes much more longer.\n",
        "a. Gaming.\nb. Quoting.\nc. Running.\n"
    ],
    "stereotype": [
        "a. More Intelligent.\nb. Less athletic.\nc. Less Calcium.\n",
        "a. A generalization used in an attempt to describe people or things.\nb. An eating habit.\nc. Cool clothes yeah.\n"
    ],
    "foreshadowing":[
        "a. Sometimes, a future event is mentioned in the story.\nb. The end of a story.\nc. The build up of the story.\n",
        "a. As a conclusion to the story.\nb. to give an indication to what will come later into the story.\nc. A way to paraphrase the text in your own words.\n"
    ],
    "personification": [
        "a. He was dead after all. No-one knew for 10 years.\nb. As she crashed into the compound, she had an adrenaline rush.\nc. The animal in question had bipedal legs like a human and similar features of a human.\n",
        "a. To give the viewer a sense of direction.\nb. To have the reader understand the object or animal in a better way.\nc. To make the viewer feel unsettled by suspension.\n"
    ],
    "interview": [
        "a. Greet the interviewer with a firm handshake and introduce yourself.\nb. Go to a hotdog stand and talk about Sonic the Hedgehog.\nc. Fall asleep when you get to the interviewers room.\n",
        "a. Exchanging information about how much swag you got.\nb. A meeting of people face to face, especially for consulation.\nc. Getting pizza from a pizza parlor.\n"
    ],
    "non": [
        "a. Use it as an english formative, usually with a negative force or absence of something.\nb. Without concious thinking or moving.\nc. To make the word sound smart, but really isn't.\n",
        "a. Nonexistent.\nb. Intelligence.\nc. Dreadful.\n"
    ],
    "less": [
        "a. Karma\nb. Idiosyncrasy.\nc. Endless.\n",
        "a. A get out of jail free card.\nb. an adjective suffix " + "meaning" + " without.\nc. A noun of legislative.\n"
    ]
}

tenwordanswers = { # contains all correct lettes
    "caption": ["b", "a"],
    "synonym": ["a", "c"],
    "salutation": ["b", "a"],
    "paraphrase": ["a", "b"],
    "stereotype": ["a", "a"],
    "foreshadowing": ["a", "b"],
    "personification": ["c", "b"],
    "interview": ["a", "b"],
    "non": ["a", "a"],
    "less": ["c", "b"]
}

tenwordexamples = { # examples for every word
    "caption":
    "The caption under the photo of this dog reads " + color.BOLD +
    "\"Husky (left) eating a hot dog.\"" + color.END,
    "synonym":
    color.BOLD + "Intelligent" + color.END + " is a synonym of " + color.BOLD +
    "smart" + color.END + ". " + color.BOLD + "Quick" + color.END +
    " is a synonym of " + color.BOLD + "fast" + color.END + ". ",
    "salutation":
    "George wrote \"Dear Judge Johnathan\" on his letter. He needed a salutation.",
    "paraphrase":
    color.BOLD + "Actual Text:" + color.END +
    " The billionaire, who is China's Jeff Bezos, was about to go public with his latest fintech venture, Ant, in what would have been the world's largest IPO. In his speech, Ma harshly criticized China's regulatory system, noting that Chinese banks have a \"Pawnshop\" way of doing things. Since the international press got wind of the extent of the fallout from his speech, everyone has wondered: Where in the world is Jack Ma? "
    + color.BOLD + "Paraphrasing:" + color.END +
    " Jack Ma, the billionaire founder of Alibaba, has gone quiet after criticising China's regulatory system in a speech.",
    "stereotype":
    "\"Asians are all good at math.\"",
    "foreshadowing":
    "Jack found a note on his way back home. It read, \"misfortune is soon to come.\" Jack was later hit by a car.",
    "personification":
    "\"What is personification?\" asked the Maple Tree.",
    "interview":
    "\"What are your certifications?\" asked the interviewer. \"I have a PH.D in phycology.\" replied the interviewee.",
    "non":
    "The english language is " + color.BOLD + "non" + color.END + "sense",
    "less":
    "The boy was emotion" + color.BOLD + "less" + color.END
}

tenwords = [ # list of all 10 words that were used with the quiz
    "caption", "synonym", "salutation", "paraphrase", "stereotype",
    "foreshadowing", "personification", "interview", "non", "less"
]
