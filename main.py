"""
  investigation/design brief: https://docs.google.com/document/d/1qEBwzhZf4D7AOVFEm4Y5xClqHPXEaGbiT1ap8r8608Y/edit?usp=sharing

  Finds words or symbols in a list and prints the definition.  Will request a word or symbol from a user to define.  Will then find a string that matches either a word from the Marzano list, a symbol or a rule.  Prints a definition of the term entered for the user input, otherwise, it will print “No definition found”.  It will also continue to ask the user for a term, until they type “exit” or “quit”.  If no string is entered, your program will raise an error and handle it.

  Your program should also include the following, where appropriate in your program:  print(), input(), variable assignment(s), math/string/comparison/logic operators (at least 2 types), inline/block comments, at least 2 data types (string, integer, floating point number, Boolean), conditionals (if, if/else, if/elif/else), loops (while, for), nested control structures, break and/or continue (if a while loop is incorporated into your program), self-defined functions, parameters and error handling protocols (try and except).

  For ELA:  You will have to give the player an overview of punctuation and basic grammar rules using the links below:
  http://www.butte.edu/departments/cas/tipsheets/grammar/parts_of_speech.html

  https://www.thepunctuationguide.com/

  ELA 7
  Vocab List: https://drive.google.com/file/d/1tDNvxfvqptIUHWBjwNBp6KPTrPf4kaSr/view#Science
"""

import games # import games.py file
from header import fullwordlist, eladict, color # importing these functions and classes from the header.py file



def punctuation_overview():
  print(color.UNDERLINE + color.BOLD + "Punctuation Overview\n\n" + color.END) #sets the text here to be underlined and bolded

  # terminal points
  print(color.BOLD + 'Terminal Points:\n' + color.END)
  print("(.) Periods end a sentence.\n ")
  print("(!) Exclamation points end a direct question. \n")
  print("(?) Question marks are used to end an exclamatory sentence.\n ")
  print("\n")

  # pause
  input("\nPress enter to move to the next section.")
  print("\n")


  # pausing points
  print(color.BOLD + 'Pausing Points:\n' + color.END)
  print("(,) Commas are used as a slight break between different parts of a sentence.\n\n(;) Semi-colons are used between two independent clauses when a coordinating conjunction is omitted.\n\n(:) Colons are used to introduce a list of items.\n")
  print("\n")

  # pause
  input("\nPress enter to move to the next section.")
  print("\n")

  # Hyphens and dashes
  print(color.BOLD + 'Hyphens and Dashes:\n' + color.END)
  print("(-) Hyphens act as the formation of certain compound terms.\n")
  print("(–) En dashes are used to represent a span or range of numbers, dates, or time. there should be no space between the En dash and the adjacent material.\n")
  print("(—) Em dashes are best limited to two apperances per sentence. Otherwise, confusion rather than clarity is likely to result. Depending on the context, the Em dash can take the place of commas, parentheses, or colons—in each case to slightly different effect.\n")
  print("\n")

  # pause
  input("\nPress enter to move to the next section.")
  print("\n")


  # Quotations
  print(color.BOLD + 'Quotations:\n' + color.END)
  print("(?) Quotation marks are used to indicate the text is being taken from another work and being written word-for-word.\n")
  print("(. . .) Ellipses are a set of 3 periods that indicate that something has been left out.\n")
  print("([]) Brackets are used to indicate clarification or a translation of text or a change in capitalization or to add emphasis to text.\n")
  print("\n")

  # pause
  input("\nPress enter to move to the next section.")
  print("\n")

  # Other Punctation
  print(color.BOLD + 'Other Punctuation:' + color.END)
  print("(()) Parenthesis allow writers to provide additional information.\n")
  print("(') Apostrophes are used in contractions (let's, there's), plurals (book's, year's), and posessives (John's book, Timmy's oil reserve). \n")
  print("(/) Slashes are used as line breaks in poetry, used meaning per (km/h), used meaning and (JD/MBA program), used meaning or (and/or), to indicate an abreviation (w/o [without]), as a fraction, and as a conflict between two things (Paris/London, nature/nurture). \n")
  print("(<>) Angle brackets are sometimes used to enclose a web address (url) or an email. (<https://www.google.com/>)\n")
  print("({}) Braces (aka curly brackets) are used in certain programming languages, certain mathematical expressions, and some musical notation. \n")
  print("\n")

def parts_of_speech():
  print(color.UNDERLINE + color.BOLD + "Eight Parts of Speech Overview\n\n" + color.END) # this text will be bolded and underlined

  # noun
  print(color.BOLD + "NOUN" + color.END + "\n")
  print("A noun is a word for a person, place, thing, or idea.\n")
  print("Examples:")
  print(color.UNDERLINE + color.BOLD + "man, Butte College, house, happiness, etc." + color.END + "\n")
  print("The young " + color.UNDERLINE + color.BOLD + "girl " + color.END + "brought me a very long " + color.UNDERLINE + color.BOLD + "letter " + color.END + "from the " + color.UNDERLINE + color.BOLD + "teacher" + color.END + ", and then she quickly disappeared. Oh my!\n\n")

  # pause
  input("\nPress enter to move to the next section.")
  print("\n")

  # pronoun
  print(color.BOLD + "PRONOUN" + color.END + "\n")
  print("A pronoun is a word used in place of a noun.\n")
  print("Examples:")
  print(color.UNDERLINE + color.BOLD + "she, we, they, it, etc." + color.END + "\n")
  print("The young girl bought " + color.UNDERLINE + color.BOLD + "me " + color.END + " a very long letter from the teacher, and then " + color.UNDERLINE + color.BOLD + "she" + color.END + " quickly disappeared. Oh My!\n")

  # pause
  input("\nPress enter to move to the next section.")
  print("\n")

  #verb
  print(color.BOLD + "VERB" + color.END + "\n")
  print("The verb in a sentence expresses action or being.\n")
  print("Examples:")
  print(color.UNDERLINE + color.BOLD + "jump, is, write, become, etc." + color.END + "\n")
  print("The young girl " + color.UNDERLINE + color.BOLD + "brought" + color.END + " me a very long letter from the teacher, and then she quickly " + color.UNDERLINE + color.BOLD + "disappeared" + color.END + ". Oh my!\n")

  # pause
  input("\nPress enter to move to the next section.")
  print("\n")

  #adjective
  print(color.BOLD + "ADJECTIVE" + color.END + "\n")
  print("An adjective is a word used to modify or describe a noun or pronoun.\n")
  print("Examples:")
  print(color.UNDERLINE + color.BOLD + "pretty, old, blue, smart, etc." + color.END + "\n")
  print("The " + color.UNDERLINE + color.BOLD + "young" + color.END + " girl brought me a very " + color.UNDERLINE + color.BOLD + "long" + color.END + " letter from the teacher, and then she quickly disappeared. Oh my!\n")

  # pause
  input("\nPress enter to move to the next section.")
  print("\n")

  #adverb
  print(color.BOLD + "ADVERB" + color.END + "\n")
  print("An adverb describes or modifies a verb, an adjective, or another adverb, but never a noun")
  print("Examples:")
  print(color.UNDERLINE + color.BOLD + "gently, extremley, carefully, well, etc." + color.END + "\n")
  print("The young girl brought me a " + color.UNDERLINE + color.BOLD + "very" + color.END + " long letter from the teacher, and " + color.UNDERLINE + color.BOLD + "then " + color.END + " she " + color.UNDERLINE + color.BOLD + " quickly" + color.END + " disappeared. Oh my!\n")

  # pause
  input("\nPress enter to move to the next section.")
  print("\n")

  #preposition
  print(color.BOLD + "PREPOSITION" + color.END + "\n")
  print("A preposition is a word placed before a noun or pronoun to form a phrase modifying another word in a sentence.")
  print("Examples:")
  print(color.UNDERLINE + color.BOLD + "by, with, about, until, etc." + color.END + "\n")
  print("The young girl brought me a very long letter " + color.UNDERLINE + color.BOLD + "from the teacher" + color.END + " , and then she quickly disappeared. Oh my!\n")

  # pause
  input("\nPress enter to move to the next section.")
  print("\n")

  #conjunction
  print(color.BOLD + "CONJUNCTION" + color.END + "\n")
  print("A conjunction joins words, phrases, or clauses, and indicates the relationship between the elements joined.")
  print("Examples:")
  print(color.UNDERLINE + color.BOLD + "and, but, or, while, because, etc." + color.END + "\n")
  print("The young girl brought me a very long letter from the teacher, " + color.UNDERLINE + color.BOLD + "and" + color.END + " then she quickly disappeared. Oh my!\n")

  # pause
  input("\nPress enter to move to the next section.")
  print("\n")

  #interjection
  print(color.BOLD + "INTERJECTION" + color.END + "\n")
  print("An interjection is a word used to express emotion. it is often followed by an exclamation point.\n")
  print("Examples:")
  print(color.UNDERLINE + color.BOLD + "Oh! Wow! Oops! etc." + color.END + "\n")
  print("The young girl brought me a very long letter from the teacher, and then she quickly disappeared. " + color.UNDERLINE + color.BOLD + "Oh my!" + color.END + "\n")




def word_dictionary():
  print("Here are all 30 words you can choose from:\n\n" + str(fullwordlist) + "\n") # prints every word on the word list
  while True:
    userinput = input("Enter a word (or enter \"quit\" or \"exit\" to exit): ")
    if userinput == "exit" or userinput == "quit": # quits the function
      break
    elif userinput == "": # so user cant enter nothing
      print("Please enter a word (or enter \"quit\" or \"exit\" to exit).")
    elif userinput.lower() in eladict: # if the userinput is in the dictionary
      print("________________________________\n") # spacer
      print(color.BOLD + color.UNDERLINE + userinput.title() + color.END + " \n" + eladict[userinput.lower()])
      print("________________________________\n")
    else:
      print("\nNo definition found :(\n")




def main(): # this runs all other functions
  punctuation_overview();
  input("\n\nPress enter to move to the next section.")
  print("\n\n")
  parts_of_speech()
  input("\n\nPress enter to move to the next section.")
  print("\n\n")
  print("\n\n\n\n\n\n")
  word_dictionary()
  input("\n\nPress enter to move to the next section.")
  print("\n\n")

  while True:
    gamechoice = input("\n\nWhat would you like to do?\na. Guess the Word\nb. A test on 10 words\nc. The Overview\nd. Quit (a, b, c, or d?): ")
    if gamechoice.lower() == "a":
      print("\n\n\n\n\n\n")
      games.guessgame()
    elif gamechoice == "b":
      print("\n\n\n\n\n\n")
      games.quizgame()
    elif gamechoice == "c":
      punctuation_overview()
      input("\n\nPress enter to move to the next section. ")
      parts_of_speech()
    elif gamechoice == "d":
      break
    else: print("Incorrect input.")



main()
